package ProyectoUnidad_5;

import java.util.Scanner;
import java.util.Arrays;

public class Metodos {

    public static Scanner scan = new Scanner(System.in);

    //METODOS DE ORDENAMIENTO INTERNOS
    public int[] Burbuja(int[] arreglo) {
        for (int i = 1; i < arreglo.length; i++) {
            for (int j = 0; j < arreglo.length - 1; j++) {
                if (arreglo[j] > arreglo[j + 1]) {
                    int aux = arreglo[j];
                    arreglo[j] = arreglo[j + 1];
                    arreglo[j + 1] = aux;
                }
            }
        }
        return arreglo;
    }

    public int[] QuickSort(int[] arreglo, int izq, int der) {
        int piv = arreglo[izq], j = der, aux, i = 0;
        while (i < j) {
            while (arreglo[i] <= piv && i < j) {
                i++;
            }
            while (arreglo[j] > piv) {
                j--;
            }
            if (i < j) {
                aux = arreglo[i];
                arreglo[i] = arreglo[j];
                arreglo[j] = aux;
            }
        }
        arreglo[izq] = arreglo[j];
        arreglo[j] = piv;
        if (izq < j - 1) {
            QuickSort(arreglo, izq, j - 1);
        }
        if (j + 1 < der) {
            QuickSort(arreglo, j + 1, der);
        }
        Tester.Flag = true;
        return arreglo;
    }

    public int[] ShellSort(int[] arreglo) {
        int salto, aux, i;
        boolean cambios;
        for (salto = arreglo.length / 2; salto != 0; salto /= 2) {
            cambios = true;
            while (cambios) { // Mientras se intercambie algún elemento
                cambios = false;
                for (i = salto; i < arreglo.length; i++) // se da una pasada
                {
                    if (arreglo[i - salto] > arreglo[i]) { // y si están desordenados
                        aux = arreglo[i]; // se reordenan
                        arreglo[i] = arreglo[i - salto];
                        arreglo[i - salto] = aux;
                        cambios = true; // y se marca como cambio.
                    }
                }
            }
        }
        Tester.Flag = true;
        return arreglo;
    }

    public int[] radixSort(int[] arreglo) {

        int[][] np = new int[arreglo.length][2];
        int[] q = new int[0x100];
        int i, j, k, l, f = 0;
        for (k = 0; k < 4; k++) {
            for (i = 0; i < (np.length - 1); i++) {
                np[i][1] = i + 1;
            }
            
            for (i = 0; i < q.length; i++) {
                q[i] = -1;
            }
            for (f = i = 0; i < arreglo.length; i++) {
                j = ((0xFF << (k << 3)) & arreglo[i]) >> (k << 3);
                if (q[j] == -1) {
                    l = q[j] = f;
                } else {
                    l = q[j];
                    while (np[l][1] != -1) {
                        l = np[l][1];
                    }
                    np[l][1] = f;
                    l = np[l][1];
                }
                f = np[f][1];
                np[l][0] = arreglo[i];
                np[l][1] = -1;
            }
            for (l = q[i = j = 0]; i < 0x100; i++) {
                for (l = q[i]; l != -1; l = np[l][1]) {
                    arreglo[j++] = np[l][0];
                }
            }
        }
        Tester.Flag = true;
        return arreglo;
    }

    //METODOS DE ORDENAMIENTO EXTERNOS
    public int[] intercalacion(int[] array, int[] arrayB,
            int tamanio) {

        int i, j, k;
        int[] resultado = new int[tamanio];
        array = Burbuja(array.clone());
        arrayB = Burbuja(arrayB.clone());

        for (i = j = k = 0; i < array.length && j < arrayB.length; k++) {
            if (array[i] < arrayB[j]) {
                resultado[k] = array[i];
                i++;
            } else {
                resultado[k] = arrayB[j];
                j++;
            }
        }

        for (; i < array.length; i++, k++) {
            resultado[k] = array[i];
        }
        for (; j < arrayB.length; j++, k++) {
            resultado[k] = arrayB[j];
        }
        return resultado;
    }
    
    public int[] mezclaDirecta(int[] arreglo) {
        int i, j, k;
        if (arreglo.length > 1) {
            int nElementosIzq = arreglo.length / 2;
            int nElementosDer = arreglo.length - nElementosIzq;
            int arregloIzq[] = new int[nElementosIzq];
            int arregloDer[] = new int[nElementosDer];
            //copiando los elementos de la primera parte del arregloIzq
            for (i = 0; i < nElementosIzq; i++) {
                arregloIzq[i] = arreglo[i];
            }
            //copiando los elementos de la segunda parte del arregloDer
            for (i = nElementosIzq; i < nElementosIzq + nElementosDer; i++) {
                arregloDer[i - nElementosIzq] = arreglo[i];
            }
            //recursividad
            arregloIzq = mezclaDirecta(arregloIzq);
            arregloDer = mezclaDirecta(arregloDer);
            i = 0;
            j = 0;
            k = 0;
            while (arregloIzq.length != j && arregloDer.length != k) {
                if (arregloIzq[j] < arregloDer[k]) {
                    arreglo[i] = arregloIzq[j];
                    i++;
                    j++;
                } else {
                    arreglo[i] = arregloDer[k];
                    i++;
                    k++;

                }

            }
            // arreglo final
            while (arregloIzq.length != j) {
                arreglo[i] = arregloIzq[j];
                i++;
                j++;

            }
            while (arregloDer.length != k) {
                arreglo[i] = arregloDer[k];
                i++;
                k++;
            }
        }
        return arreglo;

    }

    public int[] mezclaNatural(int arreglo[]) {
        int izquierda = 0, izq = 0, derecha = arreglo.length - 1, der = derecha;
        boolean ordenado = false;
        do {
            ordenado = true;
            izquierda = 0;
            while (izquierda < derecha) {
                izq = izquierda;
                while (izq < derecha && arreglo[izq] <= arreglo[izq + 1]) {
                    izq++;
                }
                der = izq + 1;
                while (der == derecha - 1 || der < derecha && arreglo[der] <= arreglo[der + 1]) {
                    der++;
                }
                if (der <= derecha) {
                    mezclaDirecta(arreglo);
                    ordenado = false;
                }
                izquierda = izq;
            }
        } while (!ordenado);
        return arreglo;
    }

    //METODOS DE BUSQUEDA
    public int BusquedaSecuencial(int[] arreglo, int dato) {
        int posicion = -1;
        for (int i = 0; i < arreglo.length; i++) {//recorremos todo el arreglo
            if (arreglo[i] == dato) {//comparamos el elemento en el arreglo con el buscado
                posicion = i;//Si es verdadero guardamos la posicion
                posicion++;
                break;//Para el ciclo
            }
        }
        return posicion;
    }

    public int BusquedaBinaria(int[] arreglo, int dato) {
        int n = arreglo.length;
        int centro, inf = 0, sup = n - 1;
        while (inf <= sup) {
            centro = (sup + inf) / 2;
            if (arreglo[centro] == dato) {
                return centro+1;
            } else if (dato < arreglo[centro]) {
                sup = centro - 1;
            } else {
                inf = centro + 1;
            }
        }
        return -1;

    }

    public void ImprimirArreglo(int[] arreglo) {
        System.out.println(Arrays.toString(arreglo));
    }

    
}
