package ProyectoUnidad_5;

import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author josemanueltirado
 * Esta clase se inicio con el proposito de probar todos los metodos 
 * para que su función y dinamismo fuese el correcto, de aqui se toma
 * referencia de muchos metodos utilizados para el desarrollo de la interfaz.
 * 
 * Nota realizada por: José Manuel Tirado Zarate-16/04/20 Finalización del
 * proyecto.
 */

public class Tester {

    static boolean Flag = false;
    private static int[] Array = new int[0];
    private static int[] supportArray = new int[0];

    public static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {

        Metodos metodos = new Metodos();
        int opc = 0, dato;

        createNewArray(false);

        Flag = false;
        do {

            System.out.println("\n=== PUESTA A PRUEBA DE METODOS ===");
            System.out.println("-- Metodos de ordenamiento internos --");
            System.out.println("1.- Nuevo arreglo");
            System.out.println("2.- Burbuja");
            System.out.println("3.- QuickSort");
            System.out.println("4.- ShellSort");
            System.out.println("5.- RadixSort");
            System.out.println("\n-- Metodos de ordenamineto externos--");
            System.out.println("6.- Intercalación");
            System.out.println("7.- Mezcla directa");
            System.out.println("8.- Mezcla natural");
            System.out.println("\n-- Metodos de busqueda --");
            System.out.println("9.- Busqueda secuencial");
            System.out.println("10.- Busqueda binaria");

            System.out.println("12.- Salir...");
            opc = scan.nextInt();

            if (opc == 1) {
                createNewArray(false);
            }
            switch (opc) {
                case 2:
                    //BURBUJA
                    Array = metodos.Burbuja(Array);
                    metodos.ImprimirArreglo(Array);
                    break;
                case 3:
                    //QUICKSORT
                    Array = metodos.QuickSort(Array, 0, Array.length - 1);
                    metodos.ImprimirArreglo(Array);
                    break;
                case 4:
                    //SHELLSORT
                    Array = metodos.ShellSort(Array);
                    metodos.ImprimirArreglo(Array);
                    break;
                case 5:
                    //RADIXSORT
                    System.out.println("Este es el arreglo: " + Arrays.toString(Array));
                    metodos.ImprimirArreglo(metodos.radixSort(Array.clone()));
                    System.out.println("Este es el arreglo (postOrdenamiento): "
                            + Arrays.toString(Array));
                    break;
                case 6:
                    //INTERCALACIÓN 
                    createNewArray(true);
                    int resultArrayLenght = Array.length + supportArray.length;
                    int[] resultArray = metodos.intercalacion(Array, supportArray,
                            resultArrayLenght);
                    metodos.ImprimirArreglo(resultArray);
                    break;
                case 7:
                    //MEZCLA DIRECTA
                    Array = metodos.mezclaDirecta(Array);
                    metodos.ImprimirArreglo(Array);
                    break;
                case 8:
                    //MEZCLA NATURAL
                    Array = metodos.mezclaNatural(Array);
                    metodos.ImprimirArreglo(Array);
                    break;
                case 9:
                    //BUSQUEDA SECUENCIAL
                    if (Flag == true) {
                        System.out.println("\nDigite el dato a encontrar: ");
                        dato = scan.nextInt();
                        dato = metodos.BusquedaSecuencial(Array, dato);
                        if (dato == -1) {
                            System.out.println("\nNo se encontro el dato");
                        } else {
                            System.out.println("\nLa posición del dato es:" + (dato + 1));
                        }
                    } else {
                        System.out.println("\nPrimero debe estar ordenado el arreglo");
                    }
                    break;
                case 10:
                    if (Flag == true) {
                        System.out.println("\nDigite el dato a encontrar: ");
                        dato = scan.nextInt();
                        dato = metodos.BusquedaBinaria(Array, dato);
                        if (dato == -1) {
                            System.out.println("\nNo se encontro el dato");
                        } else {
                            System.out.println("\nLa posición del dato es: " + (dato + 1));
                        }
                    } else {
                        System.out.println("\nPrimero debe estar ordenado el arreglo");
                    }
                    break;
                case 11:
                    //BUSQUEDA HASH

                    break;
                case 12:
                    System.out.println("Adios vuelva pronto para perder "
                            + "el tiempo");
                    break;
            }
        } while (opc != 12);
    }
    
    private static void createNewArray(boolean saveInDistinctVariable) {
        System.out.println("\nIngrese el tamaño del arreglo");
        int tamaño = scan.nextInt();

        if (!saveInDistinctVariable) {
            Array = new int[tamaño];
            System.out.println("Llene el arreglo");
            for (int i = 0; i < Array.length; i++) {
                Array[i] = scan.nextInt();
            }
        } else {
            supportArray = new int[tamaño];
            System.out.println("Llene el arreglo");
            for (int i = 0; i < supportArray.length; i++) {
                supportArray[i] = scan.nextInt();
            }
        }
        Flag = false;
    }

}
